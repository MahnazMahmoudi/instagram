package com.kurdestanbootcamp.instagram.follower;


import com.kurdestanbootcamp.instagram.comment.Comment;
import com.kurdestanbootcamp.instagram.common.BaseEntity;
import com.kurdestanbootcamp.instagram.follower_following.FollowerFollowing;
import com.kurdestanbootcamp.instagram.user.User;
import lombok.Data;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "tbl_follower")
@Data
@Audited
public class Follower extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(fetch = FetchType.LAZY,mappedBy = "follower",cascade = CascadeType.ALL)
    private List<FollowerFollowing> followerFollowings;
}
